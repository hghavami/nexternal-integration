/*
 * Created on Mar 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.nexternal.integration.NexternalCategoryTO;
import com.usatoday.nexternal.interfaces.NexternalCategoryIntf;

/**
 * @author aeast
 * @date Mar 9, 2006
 * @class NexternalCategoryDAO
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class NexternalCategoryDAO {
    // select everthing
    private static final String SELECT_ALL = "select CATEGORY, VISIBILITY, THUMBNAIL, THUMBNAIL_REDUCE, THUMBNAIL_ONLY, CATEGORY_ORDER, ACTIVE from NEXTERNAL_CATEGORY_LOAD";

    public static final String INSERT_CATEGORY = "insert into NEXTERNAL_CATEGORY_LOAD (CATEGORY, VISIBILITY, THUMBNAIL, THUMBNAIL_REDUCE, THUMBNAIL_ONLY, CATEGORY_ORDER, ACTIVE) values " +
    "(?, ?, ?, ?, ?, ?, ?);";
    
    
    private static Collection objectFactory(ResultSet rs) throws Exception {
        Collection records = new ArrayList();
        try {
            while (rs.next()){
            
                NexternalCategoryIntf product = new NexternalCategoryTO();
                
                String tempStr = null; 
                
                tempStr = rs.getString("CATEGORY");
                if (tempStr != null) {
                    product.setCategory(tempStr.trim());
                }

                tempStr = rs.getString("VISIBILITY");
                if (tempStr != null) {
                    product.setVisibility(tempStr.trim());
                }
                
                tempStr = rs.getString("THUMBNAIL");
                if (tempStr != null) {
                    product.setThumbNail(tempStr.trim());
                }
                
                tempStr = rs.getString("THUMBNAIL_REDUCE");
                if (tempStr != null) {
                    product.setThumbNailReduce(tempStr.trim());
                }
                
                tempStr = rs.getString("THUMBNAIL_ONLY");
                if (tempStr != null) {
                    product.setThumbNailOnly(tempStr.trim());
                }

                tempStr = rs.getString("CATEGORY_ORDER");
                if (tempStr != null) {
                    product.setCategoryOrder(tempStr.trim());
                }
                
                tempStr = rs.getString("ACTIVE");
                if (tempStr != null) {
                    product.setActive(tempStr.trim());
                }

                records.add(product);
            }
        }
        catch (SQLException sqle){
            throw new Exception(sqle.getMessage() + "     " + sqle.toString());
        }
        return records;
    }
    
    /**
     * 
     * @return
     * @throws Exception
     */
    public Collection fetchAll() throws Exception {
        
        Collection records = null;
       	java.sql.Connection conn = null;
    	try {

    		conn = DataSourceConnectionFactory.getDBConnection();

    		java.sql.PreparedStatement statement = conn.prepareStatement(NexternalCategoryDAO.SELECT_ALL);

   		    ResultSet rs = statement.executeQuery();
   		    
   		    records = NexternalCategoryDAO.objectFactory(rs);
   		    
    	}
    	catch (Exception e) {
            System.out.println("NexternalCategoryDAO:fetchAll() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        DataSourceConnectionFactory.closeConnection(conn);
    	    }
    	}
                
        return records;
    }

    /**
     * 
     * @param product
     * @throws Exception
     */
    public void insert(NexternalCategoryIntf cat) throws Exception {
        java.sql.Connection conn = null;
	    try {
	
	        conn = DataSourceConnectionFactory.getDBConnection();
	
	        java.sql.PreparedStatement statement = conn.prepareStatement(NexternalCategoryDAO.INSERT_CATEGORY);

	        statement = this.prepareStatementForInsert(statement, cat);
	        	    
	        // execute the SQL
	        boolean result = statement.execute();
	
	        if (statement.getUpdateCount()==0) {
	            throw new Exception("NexternalCategoryDAO: No Records Inserted and yet no exception condition exists");
	        }
	
	        statement.close();
	
	    }
	    //catch (java.sql.SQLException e) {
	    //    e.printStackTrace();
	    //    System.out.println( e.getMessage());
	     //   throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
	    //}
	    catch (Exception e) {
	        e.printStackTrace();
	        System.out.println( e.getMessage());
	        throw e;
	    }
	    finally {
	        DataSourceConnectionFactory.closeConnection(conn);
	    }        
    }
    
    /**
     * 
     * @param products - Collection of NexternalProductIntf objects
     * @throws Exception
     */
    public void bulkInsert(Collection products) throws Exception {
        java.sql.Connection conn = null;
        
        if (products == null || products.size() == 0) {
            return;
        }
        
	    try {
	
	        long startTime = System.currentTimeMillis();
	        conn = DataSourceConnectionFactory.getDBConnection();
	
	        java.sql.PreparedStatement statement = conn.prepareStatement(NexternalCategoryDAO.INSERT_CATEGORY);

	        Iterator itr = products.iterator();
	        int count = 0;
	        while(itr.hasNext()) {
	            
	            NexternalCategoryIntf cat = (NexternalCategoryIntf)itr.next();
	            statement = this.prepareStatementForInsert(statement, cat);
	         
	            statement.addBatch();

	            count++;
	            
	            // execute the SQL
//		        boolean returnValue = statement.execute();
		
//	            statement.clearParameters();
	        }
	        
	        statement.executeBatch();
	        
	        statement.close();
	
	    }
	    //catch (java.sql.SQLException e) {
	    //    e.printStackTrace();
	    //    System.out.println( e.getMessage());
	     //   throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
	    //}
	    catch (Exception e) {
	        e.printStackTrace();
	        System.out.println("bulkInsert failed: " + e.getMessage());
	        throw e;
	    }
	    finally {
	        DataSourceConnectionFactory.closeConnection(conn);
	    }                
    }

    /**
     * 
     * @param statement
     * @return
     */
    private PreparedStatement prepareStatementForInsert(PreparedStatement statement, NexternalCategoryIntf cat) throws Exception {
        
        // CATEGORY, VISIBILITY, THUMBNAIL, THUMBNAIL_REDUCE, 
        // THUMBNAIL_ONLY, CATEGORY_ORDER, ACTIVE
        statement.setString(1, cat.getCategory());
        statement.setString(2, cat.getVisibility());
        statement.setString(3, cat.getThumbNail());
        statement.setString(4, cat.getThumbNailReduce());
        statement.setString(5, cat.getThumbNailOnly());
        statement.setString(6, cat.getCategoryOrder());
        statement.setString(7, cat.getActive());
        
        return statement;
    }

}
