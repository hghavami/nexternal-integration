/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.nexternal.persistence;

import java.sql.Connection;
import java.sql.DriverManager;


/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public final class DataSourceConnectionFactory {
	
    
//    private static int openedConns = 0;
  //  private static int closedConns = 0;

	/**
	 * 
	 */
	private DataSourceConnectionFactory() {
		super();
	}

	public static synchronized java.sql.Connection getDBConnection() throws Exception {
		
	    java.sql.Connection conn = null;
	    
		try {
		    conn = DataSourceConnectionFactory.getJDBCDataSource();
		}
		catch (Exception dbe) {
			System.out.println(dbe.getMessage());
			throw dbe;				
		}
		return conn;
	}

	/**
	 * 
	 * @param userID
	 * @param password
	 * @return
	 * @throws UsatDBConnectionException
	 */
	public static synchronized java.sql.Connection getDBConnection(String userID, String password) throws Exception {

	    java.sql.Connection conn = null;

		if (userID == null || password == null ) {
		    return DataSourceConnectionFactory.getDBConnection();
		}
		
		try {
			conn = DataSourceConnectionFactory.getJDBCDataSource();
            				
            //DataSourceConnectionFactory.openedConns++;
            //System.out.println("Opened Count: " + DataSourceConnectionFactory.openedConns);
            
		}
		catch (Exception dbe) {
			System.out.println(dbe.getMessage());
			throw dbe;
		}
		return conn;
	}

	public static void closeConnection(Connection conn) {
		try {
			if (conn != null) {
				if(!conn.isClosed()){
					conn.close();
                   // DataSourceConnectionFactory.closedConns++;
                   // System.out.println("Closed Count: " + DataSourceConnectionFactory.closedConns);
				}
                //else {
                    // conn closed already
                  //  DataSourceConnectionFactory.closedConns++;
                   // System.out.println("Closed Count: " + DataSourceConnectionFactory.closedConns);
                //}
			}
		}
		catch (Exception e){
			System.out.println("Exception closing db connection: " + e.getMessage());
		}
	}
	
	private static java.sql.Connection getJDBCDataSource()throws Exception {
	    java.sql.Connection conn = null;
		try {
			// load the Microsoft JDBC driver
			// Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
			//conn = DriverManager.getConnection("jdbc:microsoft:sqlserver://usat-db1;DatabaseName=PowerBuilderDB", "esub", "Ueat12");
		    
			// load the jtds JDBC driver
		    Class.forName("net.sourceforge.jtds.jdbc.Driver");
		    conn = DriverManager.getConnection("jdbc:jtds:sqlserver://usat-db1/PowerBuilderDB;user=esub;password=Ueat12;progName=NxtrnlProdLoad");
		    //jdbc:jtds:<server_type>://<server>[:<port>][/<database>][;<property>=<value>[;...]]
		}
		catch (Exception ee) {
			throw ee;
		}				
		return conn;
	}

}
