/*
 * Created on Mar 8, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.nexternal.integration.NexternalProductTO;
import com.usatoday.nexternal.interfaces.NexternalProductIntf;

/**
 * @author aeast
 * @date Mar 8, 2006
 * @class NexternalProductDAO
 * 
 * This class manages the persistence of the Nexternal Products.
 * 
 */
public class NexternalProductDAO {
    // select everthing
    private static final String SELECT_ALL = "select PRODUCT_NO, PRODUCT_NAME, SKU, PRICING_MODEL, PRICE, DISCOUNT, CATEGORY, VENDOR, SHIP_FROM, SHORT_DESCRIPTION, LONG_DESCRIPTION, INTERNAL_MEMO, KEYWORDS, STATUS, WEIGHT, VISIBILITY, MAIN_IMAGE, MAIN_IMAGE_REDUCE, THUMBNAIL_IMAGE, THUMBNAIL_IMAGE_REDUCE, THUMBNAIL_ONLY, LARGE_IMAGE, LARGE_IMAGE_REDUCE, AUDIO_FILE, AUDIO_REPEAT, MINIMUM_QUANTITY, MAXIMUM_QUANTITY, HIDE_PRICE, CATEGORY_ORDER, INVENTORY, COGS, RESTRICTED, EXPEDITED, TAXABLE, REVIEWABLE, ALLOW_QUESTIONS, QB_SALES_ACCT, YAHOO_CATEGORY, WARNING_LEVEL, DEPLETION_STATUS, LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, SHIP_SINGLE, PACKAGE_LENGTH, PACKAGE_WIDTH, PACKAGE_HEIGHT, UPS_AH, FEDEX_NS, FEDEX_ALCOHOL, FEDEX_ALCOHOL_VOLUME, FEDEX_ALCOHOL_TYPE, FEDEX_ALCOHOL_PACKAGES, FEDEX_ALCOHOL_PACKAGING, CUSTOM_FIELD1, CUSTOM_FIELD2, CUSTOM_FIELD3, CUSTOM_FIELD4, CUSTOM_FIELD5, CUSTOM_FIELD6 from NEXTERNAL_PRODUCT_LOAD";

    public static final String INSERT_PRODUCT = "insert into NEXTERNAL_PRODUCT_LOAD (PRODUCT_NO, PRODUCT_NAME, SKU, PRICING_MODEL, PRICE, DISCOUNT, CATEGORY, VENDOR, SHIP_FROM, SHORT_DESCRIPTION, LONG_DESCRIPTION, INTERNAL_MEMO, KEYWORDS, STATUS, WEIGHT, VISIBILITY, MAIN_IMAGE, MAIN_IMAGE_REDUCE, THUMBNAIL_IMAGE, THUMBNAIL_IMAGE_REDUCE, THUMBNAIL_ONLY, LARGE_IMAGE, LARGE_IMAGE_REDUCE, AUDIO_FILE, AUDIO_REPEAT, MINIMUM_QUANTITY, MAXIMUM_QUANTITY, HIDE_PRICE, CATEGORY_ORDER, INVENTORY, COGS, RESTRICTED, EXPEDITED, TAXABLE, REVIEWABLE, ALLOW_QUESTIONS, QB_SALES_ACCT, YAHOO_CATEGORY, WARNING_LEVEL, DEPLETION_STATUS, LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, SHIP_SINGLE, PACKAGE_LENGTH, PACKAGE_WIDTH, PACKAGE_HEIGHT, UPS_AH, FEDEX_NS, FEDEX_ALCOHOL, FEDEX_ALCOHOL_VOLUME, FEDEX_ALCOHOL_TYPE, FEDEX_ALCOHOL_PACKAGES, FEDEX_ALCOHOL_PACKAGING, CUSTOM_FIELD1, CUSTOM_FIELD2, CUSTOM_FIELD3, CUSTOM_FIELD4, CUSTOM_FIELD5, CUSTOM_FIELD6) values " +
    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    
    
    private static Collection objectFactory(ResultSet rs) throws Exception {
        Collection records = new ArrayList();
        try {
            while (rs.next()){
            
                NexternalProductIntf product = new NexternalProductTO();
                
                String tempStr = null; 
                // PRODUCT_NO, PRODUCT_NAME, SKU, PRICING_MODEL, PRICE, DISCOUNT, CATEGORY, 
                // VENDOR, SHIP_FROM, SHORT_DESCRIPTION, LONG_DESCRIPTION, INTERNAL_MEMO, 
                // KEYWORDS, STATUS, WEIGHT, VISIBILITY, IMAGE1_FILE, IMAGE2_FILE, IMAGE3_FILE, 
                // MAIN_IMAGE, MAIN_IMAGE_REDUCE, THUMBNAIL_IMAGE, THUMBNAIL_IMAGE_REDUCE, 
                // THUMBNAIL_ONLY, LARGE_IMAGE, LARGE_IMAGE_REDUCE, AUDIO_FILE, AUDIO_REPEAT, 
                // MINIMUM_QUANTITY, MAXIMUM_QUANTITY, HIDE_PRICE, CATEGORY_ORDER, INVENTORY, 
                // COGS, RESTRICTED, EXPEDITED, TAXABLE, REVIEWABLE, ALLOW_QUESTIONS, 
                // QB_SALES_ACCT, YAHOO_CATEGORY, WARNING_LEVEL, DEPLETION_STATUS, 
                // LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, SHIP_SINGLE, PACKAGE_LENGTH, 
                // PACKAGE_WIDTH, PACKAGE_HEIGHT, UPS_AH, FEDEX_NS, FEDEX_ALCOHOL, FEDEX_ALCOHOL_VOLUME 
                // FEDEX_ALCOHOL_TYPE, FEDEX_ALCOHOL_PACKAGES, FEDEX_ALCOHOL_PACKAGING, CUSTOM_FIELD1, CUSTOM_FIELD2, 
                // CUSTOM_FIELD3, CUSTOM_FIELD4, CUSTOM_FIELD5, CUSTOM_FIELD6
                
                tempStr = rs.getString("PRODUCT_NO");
                if (tempStr != null) {
                    product.setProductNumber(tempStr.trim());
                }
                
                tempStr = rs.getString("PRODUCT_NAME");
                if (tempStr != null) {
                    product.setProductName(tempStr.trim());
                }
                
                tempStr = rs.getString("SKU");
                if (tempStr != null) {
                    product.setSku(tempStr.trim());
                }
                
                tempStr = rs.getString("PRICING_MODEL");
                if (tempStr != null) {
                    product.setPricingModel(tempStr.trim());
                }
                
                tempStr = rs.getString("PRICE");
                if (tempStr != null) {
                    product.setPrice(tempStr.trim());
                }
                
                tempStr = rs.getString("DISCOUNT");
                if (tempStr != null) {
                    product.setDiscount(tempStr.trim());
                }
                
                tempStr = rs.getString("CATEGORY");
                if (tempStr != null) {
                    product.setCategory(tempStr.trim());
                }
                
                tempStr = rs.getString("VENDOR");
                if (tempStr != null) {
                    product.setVendor(tempStr.trim());
                }
                
                tempStr = rs.getString("SHIP_FROM");
                if (tempStr != null) {
                    product.setShipFrom(tempStr.trim());
                }
                
                tempStr = rs.getString("SHORT_DESCRIPTION");
                if (tempStr != null) {
                    product.setShortDescription(tempStr.trim());
                }
                
                tempStr = rs.getString("LONG_DESCRIPTION");
                if (tempStr != null) {
                    product.setLongDescription(tempStr.trim());
                }
                
                tempStr = rs.getString("INTERNAL_MEMO");
                if (tempStr != null) {
                    product.setInternalMemo(tempStr.trim());
                }
                
                tempStr = rs.getString("KEYWORDS");
                if (tempStr != null) {
                    product.setKeywords(tempStr.trim());
                }
                
                tempStr = rs.getString("STATUS");
                if (tempStr != null) {
                    product.setStatus(tempStr.trim());
                }
                
                tempStr = rs.getString("WEIGHT");
                if (tempStr != null) {
                    product.setWeight(tempStr.trim());
                }
                
                tempStr = rs.getString("VISIBILITY");
                if (tempStr != null) {
                    product.setVisibility(tempStr.trim());
                }
                
                tempStr = rs.getString("MAIN_IMAGE");
                if (tempStr != null) {
                    product.setMainImage(tempStr.trim());
                }
                
                tempStr = rs.getString("MAIN_IMAGE_REDUCE");
                if (tempStr != null) {
                    product.setMainImageReduce(tempStr.trim());
                }
                
                tempStr = rs.getString("THUMBNAIL_IMAGE");
                if (tempStr != null) {
                    product.setThumbNailImage(tempStr.trim());
                }
                
                tempStr = rs.getString("THUMBNAIL_IMAGE_REDUCE");
                if (tempStr != null) {
                    product.setThumbNailImageReduce(tempStr.trim());
                }
                
                tempStr = rs.getString("THUMBNAIL_ONLY");
                if (tempStr != null) {
                    product.setThumbNailOnly(tempStr.trim());
                }
                
                tempStr = rs.getString("LARGE_IMAGE");
                if (tempStr != null) {
                    product.setLargeImage(tempStr.trim());
                }
                
                tempStr = rs.getString("LARGE_IMAGE_REDUCE");
                if (tempStr != null) {
                    product.setLargeImageReduce(tempStr.trim());
                }
                
                tempStr = rs.getString("AUDIO_FILE");
                if (tempStr != null) {
                    product.setAudioFile(tempStr.trim());
                }
                
                tempStr = rs.getString("AUDIO_REPEAT");
                if (tempStr != null) {
                    product.setAudioRepeat(tempStr.trim());
                }
                
                tempStr = rs.getString("MINIMUM_QUANTITY");
                if (tempStr != null) {
                    product.setMinimumQuantity(tempStr.trim());
                }
                
                tempStr = rs.getString("MAXIMUM_QUANTITY");
                if (tempStr != null) {
                    product.setMaximumQuantity(tempStr.trim());
                }
                
                tempStr = rs.getString("HIDE_PRICE");
                if (tempStr != null) {
                    product.setHidePrice(tempStr.trim());
                }
                
                tempStr = rs.getString("CATEGORY_ORDER");
                if (tempStr != null) {
                    product.setCategoryOrder(tempStr.trim());
                }
                
                tempStr = rs.getString("INVENTORY");
                if (tempStr != null) {
                    product.setInventory(tempStr.trim());
                }
                
                tempStr = rs.getString("COGS");
                if (tempStr != null) {
                    product.setCogs(tempStr.trim());
                }
                
                tempStr = rs.getString("RESTRICTED");
                if (tempStr != null) {
                    product.setRestricted(tempStr.trim());
                }
                
                tempStr = rs.getString("EXPEDITED");
                if (tempStr != null) {
                    product.setExpedited(tempStr.trim());
                }
                
                tempStr = rs.getString("TAXABLE");
                if (tempStr != null) {
                    product.setTaxable(tempStr.trim());
                }
                
                tempStr = rs.getString("REVIEWABLE");
                if (tempStr != null) {
                    product.setReviewable(tempStr.trim());
                }
                
                tempStr = rs.getString("ALLOW_QUESTIONS");
                if (tempStr != null) {
                    product.setAllowQuestions(tempStr.trim());
                }
                
                tempStr = rs.getString("QB_SALES_ACCT");
                if (tempStr != null) {
                    product.setQbSalesAccount(tempStr.trim());
                }
                
                tempStr = rs.getString("YAHOO_CATEGORY");
                if (tempStr != null) {
                    product.setYahooCategory(tempStr.trim());
                }
                
                tempStr = rs.getString("WARNING_LEVEL");
                if (tempStr != null) {
                    product.setWarningLevel(tempStr.trim());
                }
                
                tempStr = rs.getString("DEPLETION_STATUS");
                if (tempStr != null) {
                    product.setDepletionStatus(tempStr.trim());
                }
                
                tempStr = rs.getString("LARGE_IMAGE_WIDTH");
                if (tempStr != null) {
                    product.setLargeImageWidth(tempStr.trim());
                }
                
                tempStr = rs.getString("LARGE_IMAGE_HEIGHT");
                if (tempStr != null) {
                    product.setLargeImageHeight(tempStr.trim());
                }
                
                tempStr = rs.getString("SHIP_SINGLE");
                if (tempStr != null) {
                    product.setShipSingle(tempStr.trim());
                }
                
                tempStr = rs.getString("PACKAGE_LENGTH");
                if (tempStr != null) {
                    product.setPackageLength(tempStr.trim());
                }
                
                tempStr = rs.getString("PACKAGE_WIDTH");
                if (tempStr != null) {
                    product.setPackageWidth(tempStr.trim());
                }
                
                tempStr = rs.getString("PACKAGE_HEIGHT");
                if (tempStr != null) {
                    product.setPackageHeight(tempStr.trim());
                }
                
                tempStr = rs.getString("UPS_AH");
                if (tempStr != null) {
                    product.setUpsAH(tempStr.trim());
                }
                
                tempStr = rs.getString("FEDEX_NS");
                if (tempStr != null) {
                    product.setFedexNS(tempStr.trim());
                }
                
                tempStr = rs.getString("FEDEX_ALCOHOL");
                if (tempStr != null) {
                    product.setFedexAlcohol(tempStr.trim());
                }
                
                tempStr = rs.getString("FEDEX_ALCOHOL_VOLUME");
                if (tempStr != null) {
                    product.setFedexAlcoholVolume(tempStr.trim());
                }
                
                tempStr = rs.getString("FEDEX_ALCOHOL_TYPE");
                if (tempStr != null) {
                    product.setFedexAlcoholType(tempStr.trim());
                }
                
                tempStr = rs.getString("FEDEX_ALCOHOL_PACKAGES");
                if (tempStr != null) {
                    product.setFedexAlcoholPackages(tempStr.trim());
                }
                
                tempStr = rs.getString("FEDEX_ALCOHOL_PACKAGING");
                if (tempStr != null) {
                    product.setFedexAlcoholPackaging(tempStr.trim());
                }
                
                tempStr = rs.getString("CUSTOM_FIELD1");
                if (tempStr != null) {
                    product.setCustomField1(tempStr.trim());
                }
                
                tempStr = rs.getString("CUSTOM_FIELD2");
                if (tempStr != null) {
                    product.setCustomField2(tempStr.trim());
                }
                
                tempStr = rs.getString("CUSTOM_FIELD3");
                if (tempStr != null) {
                    product.setCustomField3(tempStr.trim());
                }
                
                tempStr = rs.getString("CUSTOM_FIELD4");
                if (tempStr != null) {
                    product.setCustomField4(tempStr.trim());
                }
                
                tempStr = rs.getString("CUSTOM_FIELD5");
                if (tempStr != null) {
                    product.setCustomField5(tempStr.trim());
                }
                
                tempStr = rs.getString("CUSTOM_FIELD6");
                if (tempStr != null) {
                    product.setCustomField6(tempStr.trim());
                }
                
                records.add(product);
            }
        }
        catch (SQLException sqle){
            throw new Exception(sqle.getMessage() + "     " + sqle.toString());
        }
        return records;
    }
    
    /**
     * 
     * @return
     * @throws Exception
     */
    public Collection fetchAll() throws Exception {
        
        Collection records = null;
       	java.sql.Connection conn = null;
    	try {

    		conn = DataSourceConnectionFactory.getDBConnection();

    		java.sql.PreparedStatement statement = conn.prepareStatement(NexternalProductDAO.SELECT_ALL);

   		    ResultSet rs = statement.executeQuery();
   		    
   		    records = NexternalProductDAO.objectFactory(rs);
   		    
    	}
    	catch (Exception e) {
            System.out.println("NexternalProductDAO:fetchAll() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        DataSourceConnectionFactory.closeConnection(conn);
    	    }
    	}
                
        return records;
    }

    /**
     * 
     * @param product
     * @throws Exception
     */
    public void insert(NexternalProductIntf product) throws Exception {
        java.sql.Connection conn = null;
	    try {
	
	        conn = DataSourceConnectionFactory.getDBConnection();
	
	        java.sql.PreparedStatement statement = conn.prepareStatement(NexternalProductDAO.INSERT_PRODUCT);

	        statement = this.prepareStatementForInsert(statement, product);
	        	    
	        // execute the SQL
	        boolean result = statement.execute();
	
	        if (statement.getUpdateCount()==0) {
	            throw new Exception("NexternalProductDAO: No Records Inserted and yet no exception condition exists");
	        }
	
	        statement.close();
	
	    }
	    //catch (java.sql.SQLException e) {
	    //    e.printStackTrace();
	    //    System.out.println( e.getMessage());
	     //   throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
	    //}
	    catch (Exception e) {
	        e.printStackTrace();
	        System.out.println( e.getMessage());
	        throw e;
	    }
	    finally {
	        DataSourceConnectionFactory.closeConnection(conn);
	    }        
    }
    
    /**
     * 
     * @param products - Collection of NexternalProductIntf objects
     * @throws Exception
     */
    public void bulkInsert(Collection products) throws Exception {
        java.sql.Connection conn = null;
        
        if (products == null || products.size() == 0) {
            return;
        }
        
	    try {
	
	        long startTime = System.currentTimeMillis();
	        conn = DataSourceConnectionFactory.getDBConnection();
	
	        java.sql.PreparedStatement statement = conn.prepareStatement(NexternalProductDAO.INSERT_PRODUCT);

	        Iterator itr = products.iterator();
	        int count = 0;
	        while(itr.hasNext()) {
	            
	            NexternalProductIntf product = (NexternalProductIntf)itr.next();
	            statement = this.prepareStatementForInsert(statement, product);
	         
	            statement.addBatch();

	            count++;
	            
	            // execute the SQL
//		        boolean returnValue = statement.execute();
		
//	            statement.clearParameters();
	        }
	        
	        statement.executeBatch();
	        
	        statement.close();
	
	    }
	    //catch (java.sql.SQLException e) {
	    //    e.printStackTrace();
	    //    System.out.println( e.getMessage());
	     //   throw new UsatException(e.getClass().getName() + " : " + e.getMessage());
	    //}
	    catch (Exception e) {
	        e.printStackTrace();
	        System.out.println("bulkInsert failed: " + e.getMessage());
	        throw e;
	    }
	    finally {
	        DataSourceConnectionFactory.closeConnection(conn);
	    }                
    }

    /**
     * 
     * @param statement
     * @return
     */
    private PreparedStatement prepareStatementForInsert(PreparedStatement statement, NexternalProductIntf product) throws Exception {
        // PRODUCT_NO, PRODUCT_NAME, SKU, PRICING_MODEL, PRICE, DISCOUNT, CATEGORY, VENDOR, SHIP_FROM, SHORT_DESCRIPTION, LONG_DESCRIPTION, 
        //INTERNAL_MEMO, KEYWORDS, STATUS, WEIGHT, VISIBILITY, 
        
        statement.setString(1, product.getProductNumber());
        statement.setString(2, product.getProductName());
        statement.setString(3, product.getSku());
        statement.setString(4, product.getPricingModel());
        statement.setString(5, product.getPrice());
        statement.setString(6, product.getDiscount());
        statement.setString(7, product.getCategory());
        statement.setString(8, product.getVendor());
        statement.setString(9, product.getShipFrom());
        statement.setString(10, product.getShortDescription());
        statement.setString(11, product.getLongDescription());
        statement.setString(12, product.getInternalMemo());
        statement.setString(13, product.getKeywords());
        statement.setString(14, product.getStatus());
        statement.setString(15, product.getWeight());
        statement.setString(16, product.getVisibility());
        
        //MAIN_IMAGE, MAIN_IMAGE_REDUCE, THUMBNAIL_IMAGE, THUMBNAIL_IMAGE_REDUCE, THUMBNAIL_ONLY, 
        // LARGE_IMAGE, LARGE_IMAGE_REDUCE, AUDIO_FILE, AUDIO_REPEAT, MINIMUM_QUANTITY, MAXIMUM_QUANTITY, HIDE_PRICE, CATEGORY_ORDER, 
        // INVENTORY, COGS, RESTRICTED, EXPEDITED, TAXABLE, REVIEWABLE, ALLOW_QUESTIONS, QB_SALES_ACCT, YAHOO_CATEGORY, WARNING_LEVEL, 
        statement.setString(17, product.getMainImage());
        statement.setString(18, product.getMainImageReduce());
        statement.setString(19, product.getThumbNailImage());
        statement.setString(20, product.getThumbNailImageReduce());
        statement.setString(21, product.getThumbNailOnly());
        statement.setString(22, product.getLargeImage());
        statement.setString(23, product.getLargeImageReduce());
        statement.setString(24, product.getAudioFile());
        statement.setString(25, product.getAudioRepeat());
        statement.setString(26, product.getMinimumQuantity());
        statement.setString(27, product.getMaximumQuantity());
        statement.setString(28, product.getHidePrice());
        statement.setString(29, product.getCategoryOrder());
        statement.setString(30, product.getInventory());
        statement.setString(31, product.getCogs());
        statement.setString(32, product.getRestricted());
        statement.setString(33, product.getExpedited());
        statement.setString(34, product.getTaxable());
        statement.setString(35, product.getReviewable());
        statement.setString(36, product.getAllowQuestions());
        statement.setString(37, product.getQbSalesAccount());
        statement.setString(38, product.getYahooCategory());
        statement.setString(39, product.getWarningLevel());
        
        // DEPLETION_STATUS, LARGE_IMAGE_WIDTH, LARGE_IMAGE_HEIGHT, SHIP_SINGLE, PACKAGE_LENGTH, PACKAGE_WIDTH, PACKAGE_HEIGHT, UPS_AH, 
        // FEDEX_NS, FEDEX_ALCOHOL, FEDEX_ALCOHOL_VOLUME, FEDEX_ALCOHOL_TYPE, FEDEX_ALCOHOL_PACKAGES, FEDEX_ALCOHOL_PACKAGING, CUSTOM_FIELD1, CUSTOM_FIELD2, CUSTOM_FIELD3, CUSTOM_FIELD4, 
        // CUSTOM_FIELD5, CUSTOM_FIELD6
        statement.setString(40, product.getDepletionStatus());
        statement.setString(41, product.getLargeImageWidth());
        statement.setString(42, product.getLargeImageHeight());
        statement.setString(43, product.getShipSingle());
        statement.setString(44, product.getPackageLength());
        statement.setString(45, product.getPackageWidth());
        statement.setString(46, product.getPackageHeight());
        statement.setString(47, product.getUpsAH());
        statement.setString(48, product.getFedexNS());
        statement.setString(49, product.getFedexAlcohol());
        statement.setString(50, product.getFedexAlcoholVolume());
        statement.setString(51, product.getFedexAlcoholType());
        statement.setString(52, product.getFedexAlcoholPackages());
        statement.setString(53, product.getFedexAlcoholPackaging());
        statement.setString(54, product.getCustomField1());
        statement.setString(55, product.getCustomField2());
        statement.setString(56, product.getCustomField3());
        statement.setString(57, product.getCustomField4());
        statement.setString(58, product.getCustomField5());
        statement.setString(59, product.getCustomField6());
        
        
        return statement;
    }
}
