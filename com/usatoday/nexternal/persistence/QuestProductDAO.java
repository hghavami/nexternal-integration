/*
 * Created on Mar 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.nexternal.integration.QuestProductTO;
import com.usatoday.nexternal.interfaces.QuestProductIntf;

/**
 * @author aeast
 * @date Mar 7, 2006
 * @class QuestProductDAO
 * 
 * This class retrieves data from the Quest Product table.
 * 
 */
public class QuestProductDAO {

    // select everthing
    private static final String SELECT_ALL = "select edition_id, pub_id, edition_date, qty_on_hand, single_copy_price, back_issue_price, public_indicator, notes from EDITION";
    private static final String SELECT_ALL_BY_PUB = "select edition_id, pub_id, edition_date, qty_on_hand, single_copy_price, back_issue_price, public_indicator, notes from EDITION where pub_id = ? order by edition_date";
    

    private static Collection objectFactory(ResultSet rs) throws Exception {
        Collection records = new ArrayList();
        try {
            while (rs.next()){
            
                QuestProductIntf product = new QuestProductTO();
                
                String tempStr = null; 
                int id = rs.getInt("edition_id");
                product.setEditionId(id);
                
                tempStr = rs.getString("pub_id");
                product.setPubId(tempStr.trim());
                
                tempStr = rs.getString("edition_date");
                product.setEditionDate(tempStr.trim());
                
                int qty = rs.getInt("qty_on_hand");
                product.setQuantity(qty);
                
                double price = rs.getDouble("single_copy_price");
                product.setSingleCopyPrice(price);
                
                double biPrice = rs.getDouble("back_issue_price");
                product.setBackIssuePrice(biPrice);
                
                tempStr = rs.getString("public_indicator");
                if (tempStr == null || "Y".equalsIgnoreCase(tempStr)) {
                    product.setPublic(true);
                }
                else {
                    product.setPublic(false);
                }
                
                tempStr = rs.getString("notes");
                if (tempStr != null) {
                    product.setNotes(tempStr.trim());
                }
                                
                records.add(product);
            }
        }
        catch (SQLException sqle){
            throw new Exception(sqle.getMessage() + "     " + sqle.toString());
        }
        return records;
    }
    
    /**
     * 
     * @return
     * @throws Exception
     */
    public Collection fetchAll() throws Exception {
        
        Collection records = null;
       	java.sql.Connection conn = null;
    	try {

    		conn = DataSourceConnectionFactory.getDBConnection();

    		java.sql.PreparedStatement statement = conn.prepareStatement(QuestProductDAO.SELECT_ALL);

   		    ResultSet rs = statement.executeQuery();
   		    
   		    records = QuestProductDAO.objectFactory(rs);
   		    
    	}
    	catch (Exception e) {
            System.out.println("QuestProductDAO:fetchAll() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        DataSourceConnectionFactory.closeConnection(conn);
    	    }
    	}
                
        return records;
    }

    /**
     * 
     * @param pub
     * @return
     * @throws Exception
     */
    public Collection fetchAllForPub(String pub) throws Exception {
        
        Collection records = null;
       	java.sql.Connection conn = null;
    	try {

    		conn = DataSourceConnectionFactory.getDBConnection();

    		java.sql.PreparedStatement statement = conn.prepareStatement(QuestProductDAO.SELECT_ALL_BY_PUB);

    		statement.setString(1, pub);
    		
   		    ResultSet rs = statement.executeQuery();
   		    
   		    records = QuestProductDAO.objectFactory(rs);
   		    
    	}
    	catch (Exception e) {
            System.out.println("QuestProductDAO:fetchAll() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        DataSourceConnectionFactory.closeConnection(conn);
    	    }
    	}
                
        return records;
    }

}
