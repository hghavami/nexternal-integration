/*
 * Created on Mar 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.nexternal.businessObjects.NexternalCategoryBO;
import com.usatoday.nexternal.businessObjects.NexternalProductBO;
import com.usatoday.nexternal.interfaces.QuestProductIntf;
import com.usatoday.nexternal.persistence.NexternalCategoryDAO;
import com.usatoday.nexternal.persistence.NexternalProductDAO;
import com.usatoday.nexternal.persistence.QuestProductDAO;

/**
 * @author aeast
 * @date Mar 7, 2006
 * @class Main
 * 
 * 
 * 
 */
public class Main {

    public static void main(String[] args) {
        
        QuestProductDAO dao = new QuestProductDAO();
        
        try {

            long start = System.currentTimeMillis();
            
            Collection records = dao.fetchAll();
            
            System.out.println("It took " + (System.currentTimeMillis() - start) + " milliseconds to query the data.");
            
            System.out.println("Number of records retrieved: " + records.size());
            System.out.println();
            
            Iterator itr = records.iterator();
            int count = 0;
            
            
            // write out sample source data
         //   System.out.println("Sample of Data...");
         //   while (itr.hasNext() && count < 10) {
         //       QuestProductIntf product = (QuestProductIntf) itr.next();
         //       
         //       System.out.println("Pub: " + product.getPubId() + " Edition: " + product.getEditionDate() + " Quantity: " + product.getQuantity());
         //       count++;
         //   }
            
            // generate business objects
            itr = records.iterator();
            count = 0;
            start = System.currentTimeMillis();
            HashMap categoryMap = new HashMap();
            ArrayList newProducts = new ArrayList();
            ArrayList badProducts = new ArrayList();
            boolean added = false;
            boolean goodProduct = false;
            int maxToProcess = 100000;
            
            int reserveCount = 0;
            
            while( itr.hasNext()) {
                QuestProductIntf product = (QuestProductIntf) itr.next();

                if (count > maxToProcess) {
                    break;
                }

                // uncomment following to skip all ut & bw products.
                //if (product.getPubId().equalsIgnoreCase("UT")|| product.getPubId().equalsIgnoreCase("BW")) {
                //    continue;
                //}
                //else {
                    count++;
                //}

                // 
                //if (product.getPubId().equalsIgnoreCase("UT") || product.getPubId().equalsIgnoreCase("BW")) {
                    
                    String editionDate = product.getEditionDate();
                    String year = editionDate.substring(0,4);
                    String month = editionDate.substring(5,7);
                    String day = editionDate.substring(8);
                    
                    DateTime pubDate = new DateTime(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), 0, 0, 0, 0 );
                    DateTime firstPubDate = new DateTime(1982, 1, 1, 0, 0, 0, 0);
                    if (pubDate.isBefore(firstPubDate.getMillis())) {
                        badProducts.add(product);
                        System.out.println("Bad Product Pub: " + product.getPubId() + " Edition: " + product.getEditionDate());
                    }
                    else {
                    
	                    NexternalProductBO nProd = new NexternalProductBO();
	                    
	                    // Subtract 10 from the quantity in Quest.
	                    int origQuantity = product.getQuantity();
	                    product.setQuantity(origQuantity - 10);
	                    
	                    boolean generateReserved = false;
	                    // if reduced quantity below threshold generate a new reserved product too
	                    if (origQuantity > 0 && origQuantity <= 10) {
	                        generateReserved = true;
	                        reserveCount++;
	                    }
	                    goodProduct = nProd.generateProductFromQuestProduct(product, false);
	                    
	                    if (goodProduct) {
	                        newProducts.add(nProd);
		                    NexternalCategoryBO nCat = new NexternalCategoryBO();
		                    if (nCat.generateCateogryForQuestProduct(product)){
		                        if (!categoryMap.containsKey(nCat.getCategory())) {
		                            categoryMap.put(nCat.getCategory(), nCat);
		                        }
		                    }
		                    
		                    if (generateReserved == true) {
		                        NexternalProductBO reservedProd = new NexternalProductBO();
		                        product.setQuantity(origQuantity);
		                        goodProduct = reservedProd.generateProductFromQuestProduct(product, true);
		                        //System.out.println("Generated Reserve Product for: " + reservedProd.getProductName() + "  SKU: " + reservedProd.getSku());
		                        if (goodProduct){
		                            newProducts.add(reservedProd);
		                        }
		                        
		                    }
	                    }
	                    else {
	                        System.out.println("Failed to generate product: " + product.getPubId() + " Edition: " + product.getEditionDate());
	                        badProducts.add(product);
	                    }
                    }
                //}
                
            }
            System.out.println("\nIt took " + (System.currentTimeMillis() - start) + " milliseconds to generate the " + newProducts.size() + " Nexternal BO objects and the " + categoryMap.size() + " categories");
            System.out.println("\n There were " + reserveCount + " Reseved products created.");
            
            start = System.currentTimeMillis();
            NexternalProductDAO nDao= new NexternalProductDAO();
            nDao.bulkInsert(newProducts);
            System.out.println("It took " + (System.currentTimeMillis() - start) + " milliseconds to persist the " + newProducts.size() + " Nexternal BO objects");
            System.out.println();

            start = System.currentTimeMillis();
            NexternalCategoryDAO ncDao= new NexternalCategoryDAO();
            ncDao.bulkInsert(categoryMap.values());
            System.out.println("It took " + (System.currentTimeMillis() - start) + " milliseconds to persist the " + categoryMap.size() + " Nexternal BO category objects");
            System.out.println();
            
            System.out.println();
            
            System.out.println("There were " + badProducts.size() + " bad/old products");
            Iterator bitr = badProducts.iterator();
            System.out.println("The followig products should be handled manually.");
            while(bitr.hasNext()) {
                QuestProductIntf qp = (QuestProductIntf)bitr.next();
                System.out.println("     Bad Product Edition: " + qp.getEditionDate() + " ID: " + qp.getEditionId() + " PUB: " + qp.getPubId());
            }
            
        
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
        System.out.println("\nApplication Terminated.");
    }
}
