/*
 * Created on Mar 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.integration;

import java.io.Serializable;

import com.usatoday.nexternal.interfaces.NexternalProductIntf;

/**
 * @author aeast
 * @date Mar 7, 2006
 * @class NexternalProductTO
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class NexternalProductTO implements Serializable, NexternalProductIntf {

    private String productNumber = "";
    private String productName = "";
    private String sku = "";
    private String pricingModel = "";
    private String price = "";
    private String discount = "";
    private String category = "";
    private String vendor = "";
    private String shipFrom = "";
    private String shortDescription = "";
    private String longDescription = "";
    private String internalMemo = "";
    private String keywords = "";
    private String status = "";
    private String weight = "";
    private String visibility = "";
    private String mainImage = "";
    private String mainImageReduce = "";
    private String thumbNailImage = "";
    private String thumbNailImageReduce = "";
    private String thumbNailOnly = "";
    private String largeImage = "";
    private String largeImageReduce = "";
    private String audioFile = "";
    private String audioRepeat = "";
    private String minimumQuantity = "";
    private String maximumQuantity = "";
    private String hidePrice = "";
    private String categoryOrder = "";
    private String inventory = "";
    private String cogs = "";
    private String restricted = "";
    private String expedited = "";
    private String taxable = "";
    private String reviewable = "";
    private String allowQuestions = "";
    private String qbSalesAccount = "";
    private String yahooCategory = "";
    private String warningLevel = "";
    private String depletionStatus = "";
    private String largeImageWidth = "";
    private String largeImageHeight = "";
    private String shipSingle = "";
    private String packageLength = "";
    private String packageWidth = "";
    private String packageHeight = "";
    private String upsAH = "";
    private String fedexNS = "";
    private String fedexAlcohol = "";
    private String fedexAlcoholVolume = "";
    private String fedexAlcoholType = "";
    private String fedexAlcoholPackages = "";
    private String fedexAlcoholPackaging = "";
    private String customField1 = "";
    private String customField2 = "";
    private String customField3 = "";
    private String customField4 = "";
    private String customField5 = "";
    private String customField6 = "";
    
    public String getAllowQuestions() {
        return this.allowQuestions;
    }
    public void setAllowQuestions(String allowQuestions) {
        this.allowQuestions = allowQuestions;
    }
    public String getAudioFile() {
        return this.audioFile;
    }
    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }
    public String getAudioRepeat() {
        return this.audioRepeat;
    }
    public void setAudioRepeat(String audioRepeat) {
        this.audioRepeat = audioRepeat;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryOrder() {
        return this.categoryOrder;
    }
    public void setCategoryOrder(String categoryOrder) {
        this.categoryOrder = categoryOrder;
    }
    public String getCogs() {
        return this.cogs;
    }
    public void setCogs(String cogs) {
        this.cogs = cogs;
    }
    public String getCustomField1() {
        return this.customField1;
    }
    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }
    public String getCustomField2() {
        return this.customField2;
    }
    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }
    public String getCustomField3() {
        return this.customField3;
    }
    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }
    public String getCustomField4() {
        return this.customField4;
    }
    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }
    public String getCustomField5() {
        return this.customField5;
    }
    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }
    public String getCustomField6() {
        return this.customField6;
    }
    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }
    public String getDepletionStatus() {
        return this.depletionStatus;
    }
    public void setDepletionStatus(String depletionStatus) {
        this.depletionStatus = depletionStatus;
    }
    public String getDiscount() {
        return this.discount;
    }
    public void setDiscount(String discount) {
        this.discount = discount;
    }
    public String getExpedited() {
        return this.expedited;
    }
    public void setExpedited(String expedited) {
        this.expedited = expedited;
    }
    public String getFedexAlcohol() {
        return this.fedexAlcohol;
    }
    public void setFedexAlcohol(String fedexAlcohol) {
        this.fedexAlcohol = fedexAlcohol;
    }
    public String getFedexAlcoholPackaging() {
        return this.fedexAlcoholPackaging;
    }
    public void setFedexAlcoholPackaging(String fedexAlcoholPackaging) {
        this.fedexAlcoholPackaging = fedexAlcoholPackaging;
    }
    public String getFedexAlcoholType() {
        return this.fedexAlcoholType;
    }
    public void setFedexAlcoholType(String fedexAlcoholType) {
        this.fedexAlcoholType = fedexAlcoholType;
    }
    public String getFedexNS() {
        return this.fedexNS;
    }
    public void setFedexNS(String fedexNS) {
        this.fedexNS = fedexNS;
    }
    public String getHidePrice() {
        return this.hidePrice;
    }
    public void setHidePrice(String hidePrice) {
        this.hidePrice = hidePrice;
    }
    public String getInternalMemo() {
        return this.internalMemo;
    }
    public void setInternalMemo(String internalMemo) {
        this.internalMemo = internalMemo;
    }
    public String getInventory() {
        return this.inventory;
    }
    public void setInventory(String inventory) {
        this.inventory = inventory;
    }
    public String getKeywords() {
        return this.keywords;
    }
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
    public String getLargeImage() {
        return this.largeImage;
    }
    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }
    public String getLargeImageHeight() {
        return this.largeImageHeight;
    }
    public void setLargeImageHeight(String largeImageHeight) {
        this.largeImageHeight = largeImageHeight;
    }
    public String getLargeImageReduce() {
        return this.largeImageReduce;
    }
    public void setLargeImageReduce(String largeImageReduce) {
        this.largeImageReduce = largeImageReduce;
    }
    public String getLargeImageWidth() {
        return this.largeImageWidth;
    }
    public void setLargeImageWidth(String largeImageWidth) {
        this.largeImageWidth = largeImageWidth;
    }
    public String getLongDescription() {
        return this.longDescription;
    }
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
    public String getMainImage() {
        return this.mainImage;
    }
    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }
    public String getMainImageReduce() {
        return this.mainImageReduce;
    }
    public void setMainImageReduce(String mainImageReduce) {
        this.mainImageReduce = mainImageReduce;
    }
    public String getMaximumQuantity() {
        return this.maximumQuantity;
    }
    public void setMaximumQuantity(String maximumQuantity) {
        this.maximumQuantity = maximumQuantity;
    }
    public String getMinimumQuantity() {
        return this.minimumQuantity;
    }
    public void setMinimumQuantity(String minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }
    public String getPackageHeight() {
        return this.packageHeight;
    }
    public void setPackageHeight(String packageHeight) {
        this.packageHeight = packageHeight;
    }
    public String getPackageLength() {
        return this.packageLength;
    }
    public void setPackageLength(String packageLength) {
        this.packageLength = packageLength;
    }
    public String getPackageWidth() {
        return this.packageWidth;
    }
    public void setPackageWidth(String packageWidth) {
        this.packageWidth = packageWidth;
    }
    public String getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getPricingModel() {
        return this.pricingModel;
    }
    public void setPricingModel(String pricingModel) {
        this.pricingModel = pricingModel;
    }
    public String getProductName() {
        return this.productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductNumber() {
        return this.productNumber;
    }
    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }
    public String getQbSalesAccount() {
        return this.qbSalesAccount;
    }
    public void setQbSalesAccount(String qbSalesAccount) {
        this.qbSalesAccount = qbSalesAccount;
    }
    public String getRestricted() {
        return this.restricted;
    }
    public void setRestricted(String restricted) {
        this.restricted = restricted;
    }
    public String getReviewable() {
        return this.reviewable;
    }
    public void setReviewable(String reviewable) {
        this.reviewable = reviewable;
    }
    public String getShipFrom() {
        return this.shipFrom;
    }
    public void setShipFrom(String shipFrom) {
        this.shipFrom = shipFrom;
    }
    public String getShipSingle() {
        return this.shipSingle;
    }
    public void setShipSingle(String shipSingle) {
        this.shipSingle = shipSingle;
    }
    public String getShortDescription() {
        return this.shortDescription;
    }
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    public String getSku() {
        return this.sku;
    }
    public void setSku(String sku) {
        this.sku = sku;
    }
    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getTaxable() {
        return this.taxable;
    }
    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }
    public String getThumbNailImage() {
        return this.thumbNailImage;
    }
    public void setThumbNailImage(String thumbNailImage) {
        this.thumbNailImage = thumbNailImage;
    }
    public String getThumbNailImageReduce() {
        return this.thumbNailImageReduce;
    }
    public void setThumbNailImageReduce(String thumbNailImageReduce) {
        this.thumbNailImageReduce = thumbNailImageReduce;
    }
    public String getThumbNailOnly() {
        return this.thumbNailOnly;
    }
    public void setThumbNailOnly(String thumbNailOnly) {
        this.thumbNailOnly = thumbNailOnly;
    }
    public String getUpsAH() {
        return this.upsAH;
    }
    public void setUpsAH(String upsAH) {
        this.upsAH = upsAH;
    }
    public String getVendor() {
        return this.vendor;
    }
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    public String getVisibility() {
        return this.visibility;
    }
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
    public String getWarningLevel() {
        return this.warningLevel;
    }
    public void setWarningLevel(String warningLevel) {
        this.warningLevel = warningLevel;
    }
    public String getWeight() {
        return this.weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getYahooCategory() {
        return this.yahooCategory;
    }
    public void setYahooCategory(String yahooCategory) {
        this.yahooCategory = yahooCategory;
    }
    public String getFedexAlcoholPackages() {
        return this.fedexAlcoholPackages;
    }
    public void setFedexAlcoholPackages(String fedexAlcoholPackages) {
        this.fedexAlcoholPackages = fedexAlcoholPackages;
    }
    public String getFedexAlcoholVolume() {
        return this.fedexAlcoholVolume;
    }
    public void setFedexAlcoholVolume(String fedexAlcoholVolume) {
        this.fedexAlcoholVolume = fedexAlcoholVolume;
    }
}
