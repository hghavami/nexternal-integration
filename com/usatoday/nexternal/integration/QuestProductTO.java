/*
 * Created on Mar 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.integration;

import java.io.Serializable;

import com.usatoday.nexternal.interfaces.QuestProductIntf;

/**
 * @author aeast
 * @date Mar 7, 2006
 * @class QuestProductTO
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class QuestProductTO implements QuestProductIntf, Serializable {
    private int editionId = -1;
    private String pubId = null;
    private String editionDate = null;
    private int quantity = -1;
    private double singleCopyPrice = 0.00;
    private double backIssuePrice = 0.00;
    private boolean isPublic = false;
    private String notes = null;
    
    public double getBackIssuePrice() {
        return this.backIssuePrice;
    }
    public void setBackIssuePrice(double backIssuePrice) {
        this.backIssuePrice = backIssuePrice;
    }
    public String getEditionDate() {
        return this.editionDate;
    }
    public void setEditionDate(String editionDate) {
        this.editionDate = editionDate;
    }
    public int getEditionId() {
        return this.editionId;
    }
    public void setEditionId(int editionId) {
        this.editionId = editionId;
    }
    public boolean isPublic() {
        return this.isPublic;
    }
    public void setPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }
    public String getNotes() {
        return this.notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public String getPubId() {
        return this.pubId;
    }
    public void setPubId(String pubId) {
        this.pubId = pubId;
    }
    public int getQuantity() {
        return this.quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public double getSingleCopyPrice() {
        return this.singleCopyPrice;
    }
    public void setSingleCopyPrice(double singleCopyPrice) {
        this.singleCopyPrice = singleCopyPrice;
    }
}
