/*
 * Created on Mar 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.integration;

import java.io.Serializable;

import com.usatoday.nexternal.interfaces.NexternalCategoryIntf;

/**
 * @author aeast
 * @date Mar 9, 2006
 * @class NexternalCategoryTO
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class NexternalCategoryTO implements Serializable, NexternalCategoryIntf {
    private String category = "";
    private String visibility = "";
    private String thumbNail = "";
    private String thumbNailOnly = "";
    private String categoryOrder = "";
    private String active = "";
    private String thumbNailReduce = "";
    

    public String getActive() {
        return this.active;
    }
    public void setActive(String active) {
        this.active = active;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryOrder() {
        return this.categoryOrder;
    }
    public void setCategoryOrder(String categoryOrder) {
        this.categoryOrder = categoryOrder;
    }
    public String getThumbNail() {
        return this.thumbNail;
    }
    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }
    public String getThumbNailOnly() {
        return this.thumbNailOnly;
    }
    public void setThumbNailOnly(String thumbNailOnly) {
        this.thumbNailOnly = thumbNailOnly;
    }
    public String getVisibility() {
        return this.visibility;
    }
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
    public String getThumbNailReduce() {
        return this.thumbNailReduce;
    }
    public void setThumbNailReduce(String thumbNailReduce) {
        this.thumbNailReduce = thumbNailReduce;
    }
}
