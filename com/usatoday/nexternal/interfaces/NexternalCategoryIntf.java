/*
 * Created on Mar 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.interfaces;

/**
 * @author aeast
 * @date Mar 9, 2006
 * @class NexternalCategoryIntf
 * 
 * TODO Add a brief description of this class.
 * 
 */
public interface NexternalCategoryIntf {
    public abstract String getActive();

    public abstract void setActive(String active);

    public abstract String getCategory();

    public abstract void setCategory(String category);

    public abstract String getCategoryOrder();

    public abstract void setCategoryOrder(String categoryOrder);

    public abstract String getThumbNail();

    public abstract void setThumbNail(String thumbNail);

    public abstract String getThumbNailOnly();

    public abstract void setThumbNailOnly(String thumbNailOnly);

    public abstract String getVisibility();

    public abstract void setVisibility(String visibility);
    
    public abstract String getThumbNailReduce();

    public abstract void setThumbNailReduce(String thumbNailReduce);
}