/*
 * Created on Mar 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.interfaces;

/**
 * @author aeast
 * @date Mar 7, 2006
 * @class QuestProductIntf
 * 
 * TODO Add a brief description of this class.
 * 
 */
public interface QuestProductIntf {
    public abstract double getBackIssuePrice();

    public abstract void setBackIssuePrice(double backIssuePrice);

    public abstract String getEditionDate();

    public abstract void setEditionDate(String editionDate);

    public abstract int getEditionId();

    public abstract void setEditionId(int editionId);

    public abstract boolean isPublic();

    public abstract void setPublic(boolean isPublic);

    public abstract String getNotes();

    public abstract void setNotes(String notes);

    public abstract String getPubId();

    public abstract void setPubId(String pubId);

    public abstract int getQuantity();

    public abstract void setQuantity(int quantity);

    public abstract double getSingleCopyPrice();

    public abstract void setSingleCopyPrice(double singleCopyPrice);
}