/*
 * Created on Mar 7, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.interfaces;

/**
 * @author aeast
 * @date Mar 7, 2006
 * @class NexternalProductIntf
 * 
 * TODO Add a brief description of this class.
 * 
 */
public interface NexternalProductIntf {
    public abstract String getAllowQuestions();

    public abstract void setAllowQuestions(String allowQuestions);

    public abstract String getAudioFile();

    public abstract void setAudioFile(String audioFile);

    public abstract String getAudioRepeat();

    public abstract void setAudioRepeat(String audioRepeat);

    public abstract String getCategory();

    public abstract void setCategory(String category);

    public abstract String getCategoryOrder();

    public abstract void setCategoryOrder(String categoryOrder);

    public abstract String getCogs();

    public abstract void setCogs(String cogs);

    public abstract String getCustomField1();

    public abstract void setCustomField1(String customField1);

    public abstract String getCustomField2();

    public abstract void setCustomField2(String customField2);

    public abstract String getCustomField3();

    public abstract void setCustomField3(String customField3);

    public abstract String getCustomField4();

    public abstract void setCustomField4(String customField4);

    public abstract String getCustomField5();

    public abstract void setCustomField5(String customField5);

    public abstract String getCustomField6();

    public abstract void setCustomField6(String customField6);

    public abstract String getDepletionStatus();

    public abstract void setDepletionStatus(String depletionStatus);

    public abstract String getDiscount();

    public abstract void setDiscount(String discount);

    public abstract String getExpedited();

    public abstract void setExpedited(String expedited);

    public abstract String getFedexAlcohol();

    public abstract void setFedexAlcohol(String fedexAlcohol);

    public abstract String getFedexAlcoholVolume();

    public abstract void setFedexAlcoholVolume(String fedexAlcoholVolume);
    
    public abstract String getFedexAlcoholPackages();

    public abstract void setFedexAlcoholPackages(String fedexAlcoholPackages);

    public abstract String getFedexAlcoholPackaging();

    public abstract void setFedexAlcoholPackaging(String fedexAlcoholPackaging);

    public abstract String getFedexAlcoholType();

    public abstract void setFedexAlcoholType(String fedexAlcoholType);

    public abstract String getFedexNS();

    public abstract void setFedexNS(String fedexNS);

    public abstract String getHidePrice();

    public abstract void setHidePrice(String hidePrice);

    public abstract String getInternalMemo();

    public abstract void setInternalMemo(String internalMemo);

    public abstract String getInventory();

    public abstract void setInventory(String inventory);

    public abstract String getKeywords();

    public abstract void setKeywords(String keywords);

    public abstract String getLargeImage();

    public abstract void setLargeImage(String largeImage);

    public abstract String getLargeImageHeight();

    public abstract void setLargeImageHeight(String largeImageHeight);

    public abstract String getLargeImageReduce();

    public abstract void setLargeImageReduce(String largeImageReduce);

    public abstract String getLargeImageWidth();

    public abstract void setLargeImageWidth(String largeImageWidth);

    public abstract String getLongDescription();

    public abstract void setLongDescription(String longDescription);

    public abstract String getMainImage();

    public abstract void setMainImage(String mainImage);

    public abstract String getMainImageReduce();

    public abstract void setMainImageReduce(String mainImageReduce);

    public abstract String getMaximumQuantity();

    public abstract void setMaximumQuantity(String maximumQuantity);

    public abstract String getMinimumQuantity();

    public abstract void setMinimumQuantity(String minimumQuantity);

    public abstract String getPackageHeight();

    public abstract void setPackageHeight(String packageHeight);

    public abstract String getPackageLength();

    public abstract void setPackageLength(String packageLength);

    public abstract String getPackageWidth();

    public abstract void setPackageWidth(String packageWidth);

    public abstract String getPrice();

    public abstract void setPrice(String price);

    public abstract String getPricingModel();

    public abstract void setPricingModel(String pricingModel);

    public abstract String getProductName();

    public abstract void setProductName(String productName);

    public abstract String getProductNumber();

    public abstract void setProductNumber(String productNumber);

    public abstract String getQbSalesAccount();

    public abstract void setQbSalesAccount(String qbSalesAccount);

    public abstract String getRestricted();

    public abstract void setRestricted(String restricted);

    public abstract String getReviewable();

    public abstract void setReviewable(String reviewable);

    public abstract String getShipFrom();

    public abstract void setShipFrom(String shipFrom);

    public abstract String getShipSingle();

    public abstract void setShipSingle(String shipSingle);

    public abstract String getShortDescription();

    public abstract void setShortDescription(String shortDescription);

    public abstract String getSku();

    public abstract void setSku(String sku);

    public abstract String getStatus();

    public abstract void setStatus(String status);

    public abstract String getTaxable();

    public abstract void setTaxable(String taxable);

    public abstract String getThumbNailImage();

    public abstract void setThumbNailImage(String thumbNailImage);

    public abstract String getThumbNailImageReduce();

    public abstract void setThumbNailImageReduce(String thumbNailImageReduce);

    public abstract String getThumbNailOnly();

    public abstract void setThumbNailOnly(String thumbNailOnly);

    public abstract String getUpsAH();

    public abstract void setUpsAH(String upsAH);

    public abstract String getVendor();

    public abstract void setVendor(String vendor);

    public abstract String getVisibility();

    public abstract void setVisibility(String visibility);

    public abstract String getWarningLevel();

    public abstract void setWarningLevel(String warningLevel);

    public abstract String getWeight();

    public abstract void setWeight(String weight);

    public abstract String getYahooCategory();

    public abstract void setYahooCategory(String yahooCategory);
}