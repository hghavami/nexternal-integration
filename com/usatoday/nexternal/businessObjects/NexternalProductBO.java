/*
 * Created on Mar 8, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.businessObjects;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import com.usatoday.nexternal.interfaces.NexternalProductIntf;
import com.usatoday.nexternal.interfaces.QuestProductIntf;

/**
 * @author aeast
 * @date Mar 8, 2006
 * @class NexternalProductBO
 * 
 * 
 */
public class NexternalProductBO implements NexternalProductIntf {

    private String productNumber = "";
    private String productName = "";
    private String sku = "";
    private String pricingModel = "";
    private String price = "";
    private String discount = "";
    private String category = "";
    private String vendor = "";
    private String shipFrom = "";
    private String shortDescription = "";
    private String longDescription = "";
    private String internalMemo = "";
    private String keywords = "";
    private String status = "";
    private String weight = "";
    private String visibility = "";
    private String mainImage = "";
    private String mainImageReduce = "";
    private String thumbNailImage = "";
    private String thumbNailImageReduce = "";
    private String thumbNailOnly = "";
    private String largeImage = "";
    private String largeImageReduce = "";
    private String audioFile = "";
    private String audioRepeat = "";
    private String minimumQuantity = "";
    private String maximumQuantity = "";
    private String hidePrice = "";
    private String categoryOrder = "";
    private String inventory = "";
    private String cogs = "";
    private String restricted = "";
    private String expedited = "";
    private String taxable = "";
    private String reviewable = "";
    private String allowQuestions = "";
    private String qbSalesAccount = "";
    private String yahooCategory = "";
    private String warningLevel = "";
    private String depletionStatus = "";
    private String largeImageWidth = "";
    private String largeImageHeight = "";
    private String shipSingle = "";
    private String packageLength = "";
    private String packageWidth = "";
    private String packageHeight = "";
    private String upsAH = "";
    private String fedexNS = "";
    private String fedexAlcohol = "";
    private String fedexAlcoholVolume = "";
    private String fedexAlcoholType = "";
    private String fedexAlcoholPackages = "";
    private String fedexAlcoholPackaging = "";
    private String customField1 = "";
    private String customField2 = "";
    private String customField3 = "";
    private String customField4 = "";
    private String customField5 = "";
    private String customField6 = "";
    
    // 
    private static DateTime publicAvailabilityDate = new DateTime(1988, 1, 1, 0, 0, 0, 0);
    
    public String getAllowQuestions() {
        return this.allowQuestions;
    }
    public void setAllowQuestions(String allowQuestions) {
        this.allowQuestions = allowQuestions;
    }
    public String getAudioFile() {
        return this.audioFile;
    }
    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }
    public String getAudioRepeat() {
        return this.audioRepeat;
    }
    public void setAudioRepeat(String audioRepeat) {
        this.audioRepeat = audioRepeat;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryOrder() {
        return this.categoryOrder;
    }
    public void setCategoryOrder(String categoryOrder) {
        this.categoryOrder = categoryOrder;
    }
    public String getCogs() {
        return this.cogs;
    }
    public void setCogs(String cogs) {
        this.cogs = cogs;
    }
    public String getCustomField1() {
        return this.customField1;
    }
    public void setCustomField1(String customField1) {
        this.customField1 = customField1;
    }
    public String getCustomField2() {
        return this.customField2;
    }
    public void setCustomField2(String customField2) {
        this.customField2 = customField2;
    }
    public String getCustomField3() {
        return this.customField3;
    }
    public void setCustomField3(String customField3) {
        this.customField3 = customField3;
    }
    public String getCustomField4() {
        return this.customField4;
    }
    public void setCustomField4(String customField4) {
        this.customField4 = customField4;
    }
    public String getCustomField5() {
        return this.customField5;
    }
    public void setCustomField5(String customField5) {
        this.customField5 = customField5;
    }
    public String getCustomField6() {
        return this.customField6;
    }
    public void setCustomField6(String customField6) {
        this.customField6 = customField6;
    }
    public String getDepletionStatus() {
        return this.depletionStatus;
    }
    public void setDepletionStatus(String depletionStatus) {
        this.depletionStatus = depletionStatus;
    }
    public String getDiscount() {
        return this.discount;
    }
    public void setDiscount(String discount) {
        this.discount = discount;
    }
    public String getExpedited() {
        return this.expedited;
    }
    public void setExpedited(String expedited) {
        this.expedited = expedited;
    }
    public String getFedexAlcohol() {
        return this.fedexAlcohol;
    }
    public void setFedexAlcohol(String fedexAlcohol) {
        this.fedexAlcohol = fedexAlcohol;
    }
    public String getFedexAlcoholPackaging() {
        return this.fedexAlcoholPackaging;
    }
    public void setFedexAlcoholPackaging(String fedexAlcoholPackaging) {
        this.fedexAlcoholPackaging = fedexAlcoholPackaging;
    }
    public String getFedexAlcoholType() {
        return this.fedexAlcoholType;
    }
    public void setFedexAlcoholType(String fedexAlcoholType) {
        this.fedexAlcoholType = fedexAlcoholType;
    }
    public String getFedexNS() {
        return this.fedexNS;
    }
    public void setFedexNS(String fedexNS) {
        this.fedexNS = fedexNS;
    }
    public String getHidePrice() {
        return this.hidePrice;
    }
    public void setHidePrice(String hidePrice) {
        this.hidePrice = hidePrice;
    }
    public String getInternalMemo() {
        return this.internalMemo;
    }
    public void setInternalMemo(String internalMemo) {
        this.internalMemo = internalMemo;
    }
    public String getInventory() {
        return this.inventory;
    }
    public void setInventory(String inventory) {
        this.inventory = inventory;
    }
    public String getKeywords() {
        return this.keywords;
    }
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
    public String getLargeImage() {
        return this.largeImage;
    }
    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }
    public String getLargeImageHeight() {
        return this.largeImageHeight;
    }
    public void setLargeImageHeight(String largeImageHeight) {
        this.largeImageHeight = largeImageHeight;
    }
    public String getLargeImageReduce() {
        return this.largeImageReduce;
    }
    public void setLargeImageReduce(String largeImageReduce) {
        this.largeImageReduce = largeImageReduce;
    }
    public String getLargeImageWidth() {
        return this.largeImageWidth;
    }
    public void setLargeImageWidth(String largeImageWidth) {
        this.largeImageWidth = largeImageWidth;
    }
    public String getLongDescription() {
        return this.longDescription;
    }
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
    public String getMainImage() {
        return this.mainImage;
    }
    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }
    public String getMainImageReduce() {
        return this.mainImageReduce;
    }
    public void setMainImageReduce(String mainImageReduce) {
        this.mainImageReduce = mainImageReduce;
    }
    public String getMaximumQuantity() {
        return this.maximumQuantity;
    }
    public void setMaximumQuantity(String maximumQuantity) {
        this.maximumQuantity = maximumQuantity;
    }
    public String getMinimumQuantity() {
        return this.minimumQuantity;
    }
    public void setMinimumQuantity(String minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }
    public String getPackageHeight() {
        return this.packageHeight;
    }
    public void setPackageHeight(String packageHeight) {
        this.packageHeight = packageHeight;
    }
    public String getPackageLength() {
        return this.packageLength;
    }
    public void setPackageLength(String packageLength) {
        this.packageLength = packageLength;
    }
    public String getPackageWidth() {
        return this.packageWidth;
    }
    public void setPackageWidth(String packageWidth) {
        this.packageWidth = packageWidth;
    }
    public String getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getPricingModel() {
        return this.pricingModel;
    }
    public void setPricingModel(String pricingModel) {
        this.pricingModel = pricingModel;
    }
    public String getProductName() {
        return this.productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductNumber() {
        return this.productNumber;
    }
    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }
    public String getQbSalesAccount() {
        return this.qbSalesAccount;
    }
    public void setQbSalesAccount(String qbSalesAccount) {
        this.qbSalesAccount = qbSalesAccount;
    }
    public String getRestricted() {
        return this.restricted;
    }
    public void setRestricted(String restricted) {
        this.restricted = restricted;
    }
    public String getReviewable() {
        return this.reviewable;
    }
    public void setReviewable(String reviewable) {
        this.reviewable = reviewable;
    }
    public String getShipFrom() {
        return this.shipFrom;
    }
    public void setShipFrom(String shipFrom) {
        this.shipFrom = shipFrom;
    }
    public String getShipSingle() {
        return this.shipSingle;
    }
    public void setShipSingle(String shipSingle) {
        this.shipSingle = shipSingle;
    }
    public String getShortDescription() {
        return this.shortDescription;
    }
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
    public String getSku() {
        return this.sku;
    }
    public void setSku(String sku) {
        this.sku = sku;
    }
    public String getStatus() {
        return this.status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getTaxable() {
        return this.taxable;
    }
    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }
    public String getThumbNailImage() {
        return this.thumbNailImage;
    }
    public void setThumbNailImage(String thumbNailImage) {
        this.thumbNailImage = thumbNailImage;
    }
    public String getThumbNailImageReduce() {
        return this.thumbNailImageReduce;
    }
    public void setThumbNailImageReduce(String thumbNailImageReduce) {
        this.thumbNailImageReduce = thumbNailImageReduce;
    }
    public String getThumbNailOnly() {
        return this.thumbNailOnly;
    }
    public void setThumbNailOnly(String thumbNailOnly) {
        this.thumbNailOnly = thumbNailOnly;
    }
    public String getUpsAH() {
        return this.upsAH;
    }
    public void setUpsAH(String upsAH) {
        this.upsAH = upsAH;
    }
    public String getVendor() {
        return this.vendor;
    }
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }
    public String getVisibility() {
        return this.visibility;
    }
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
    public String getWarningLevel() {
        return this.warningLevel;
    }
    public void setWarningLevel(String warningLevel) {
        this.warningLevel = warningLevel;
    }
    public String getWeight() {
        return this.weight;
    }
    public void setWeight(String weight) {
        this.weight = weight;
    }
    public String getYahooCategory() {
        return this.yahooCategory;
    }
    public void setYahooCategory(String yahooCategory) {
        this.yahooCategory = yahooCategory;
    }
    public String getFedexAlcoholPackages() {
        return this.fedexAlcoholPackages;
    }
    public void setFedexAlcoholPackages(String fedexAlcoholPackages) {
        this.fedexAlcoholPackages = fedexAlcoholPackages;
    }
    public String getFedexAlcoholVolume() {
        return this.fedexAlcoholVolume;
    }
    public void setFedexAlcoholVolume(String fedexAlcoholVolume) {
        this.fedexAlcoholVolume = fedexAlcoholVolume;
    }
    
    /**
     * 
     * @param qp
     */
    public boolean generateProductFromQuestProduct(QuestProductIntf qp, boolean isReserved) {
        if (qp == null) { 
            return false;
        }
        
        StringBuffer categoryBuf = new StringBuffer();
        StringBuffer keywordBuf = new StringBuffer();
        StringBuffer skuBuf = new StringBuffer();
        StringBuffer prodNameBuf = new StringBuffer();
        StringBuffer longDescriptionBuf = new StringBuffer();
        
        String temp = qp.getPubId();
        if (temp == null) {
            return false;
        }
        temp = temp.trim();
        
        String editionDate = qp.getEditionDate();
        String year = editionDate.substring(0,4);
        String month = editionDate.substring(5,7);
        String day = editionDate.substring(8);
        
        DateTime pubDate = new DateTime(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), 0, 0, 0, 0 );
        
        prodNameBuf.append(pubDate.toString("MM/dd/yyyy")).append(" Issue of ");

        DateTimeFormatter longDesFmt = new DateTimeFormatterBuilder().appendDayOfWeekText().appendLiteral(", ").appendMonthOfYearText().appendLiteral(" ").appendDayOfMonth(2).appendLiteral(", ").appendPattern("yyyy").toFormatter();

        skuBuf.append(pubDate.toString("yyyyMMdd")).append(temp);
        if (isReserved) {
            skuBuf.append("_RSVD");
        }

        // sample edition date 1995-08-18
        if ("UT".equalsIgnoreCase(temp)) {
            prodNameBuf.append("USA TODAY");
            categoryBuf.append("USA TODAY Previous Issues|");
            longDescriptionBuf.append(pubDate.toString(longDesFmt)).append(" Edition of USA TODAY");
            this.setMainImage("UTDefaultCoverMainImage.gif");
            this.setThumbNailImage("UTDefaultCoverThumbnailImage.gif");
            
        }
        else if ("BW".equalsIgnoreCase(temp)) {
            prodNameBuf.append("Sports Weekly");
            categoryBuf.append("Sports Weekly Previous Issues|");
            longDescriptionBuf.append(pubDate.toString(longDesFmt)).append(" Edition of Sports Weekly");
            // override SKU
            skuBuf = new StringBuffer(pubDate.toString("yyyyMMdd")).append("SW");
            this.setMainImage("SWDefaultCoverMainImage.gif");
            this.setThumbNailImage("SWDefaultCoverThumbnailImage.gif");
        }
        else if ("UW".equalsIgnoreCase(temp)) {
            categoryBuf.append("USA Weekend Previous Issues|");
            prodNameBuf.append("USA Weekend");
            longDescriptionBuf.append(pubDate.toString(longDesFmt)).append(" Edition of USA Weekend");
        }
        else if ("IN".equalsIgnoreCase(temp)) {
            categoryBuf.append("USA TODAY International Previous Issues|");
            prodNameBuf.append("USA TODAY International");
            longDescriptionBuf.append(pubDate.toString(longDesFmt)).append(" Edition of USA TODAY International");
        }
        else {
            categoryBuf.append("Special Editions Previous Issues|");
            if ("ON".equalsIgnoreCase(temp)) {
                prodNameBuf.append("On-Line USAT Report");
                keywordBuf.append("On-Line USAT Report ");
            }
            else if ("FB".equalsIgnoreCase(temp) || "FS".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Fantasy Football");
                keywordBuf.append("Fantasy Football ");
            }
            else if ("AP".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Aging Par Report 7/17/95");
            }
            else if ("SB".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Patriots Super Bowl");
                keywordBuf.append("Patriots Super Bowl ");
            }
            else if ("BA".equalsIgnoreCase(temp)) {
                prodNameBuf.append("BA");
            }
            else if ("BR".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Retirement Report 5/8/95");
            }
            else if ("CU".equalsIgnoreCase(temp)) {
                prodNameBuf.append("College Preview");
                keywordBuf.append("College Preview ");
            }
            else if ("PE".equalsIgnoreCase(temp)) {
                prodNameBuf.append("PE");
            }
            else if ("NF".equalsIgnoreCase(temp)) {
                prodNameBuf.append("NF");
            }
            else if ("TG".equalsIgnoreCase(temp)) {
                prodNameBuf.append("USA TODAY Personal Technology NOW");
                keywordBuf.append("TechNOW Personal Technology NOW ");
            }
            else if ("BB".equalsIgnoreCase(temp)) {
                prodNameBuf.append("BB");
            }
            // skip FD current subscribers
            else if (/*"FD".equalsIgnoreCase(temp) ||*/ "BD".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Fantasy Baseball Guide");
                keywordBuf.append("2006 Fantasy Baseball Guide ");
            }
            else if ("GT".equalsIgnoreCase(temp)) {
                prodNameBuf.append("GT");
            }
            else if ("WR".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Water Reprint");
            }
            else if ("CH".equalsIgnoreCase(temp)) {
                prodNameBuf.append("CH");
            }
            // skip AF - subscriber pricing
            else if ("FN".equalsIgnoreCase(temp) /*|| "AF".equalsIgnoreCase(temp)*/) {
                prodNameBuf.append("Fantasy Football Guide");
                keywordBuf.append("Fantasy Football Guide ");
            }
            else if ("CP".equalsIgnoreCase(temp)) {
                prodNameBuf.append("College Preview");
                keywordBuf.append("College Preview ");
            }
            else if ("PA".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Eagles Commemorative");
                keywordBuf.append("Eagles Philadelphia Commemorative ");
            }
            else if ("DG".equalsIgnoreCase(temp)) {
                prodNameBuf.append("DG");
            }
            else if ("ME".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Millennium Edition");
                keywordBuf.append("Millennium Edition ");
            }
            else if ("N3".equalsIgnoreCase(temp)) {
                // N4 is current subscriber
                prodNameBuf.append("NASCAR Guide");
                keywordBuf.append("2006 NASCAR Guide ");
                qp.setBackIssuePrice(4.95);
            }
            else if ("BF".equalsIgnoreCase(temp)) {
                prodNameBuf.append("BF");
            }
            else if ("NB".equalsIgnoreCase(temp)) {
                prodNameBuf.append("NB");
            }
            else if ("CR".equalsIgnoreCase(temp)) {
                prodNameBuf.append("College Rivalries");
                keywordBuf.append("College ");
            }
            else if ("FA".equalsIgnoreCase(temp)) {
                prodNameBuf.append("FA");
            }
            else if ("WS".equalsIgnoreCase(temp)) {
                prodNameBuf.append("Boston Red Sox World Series");
                keywordBuf.append("Boston Red Sox World Series ");
            }
            else if ("RS".equalsIgnoreCase(temp)) {
                prodNameBuf.append("RS");
            }
            else if ("BG".equalsIgnoreCase(temp)) {
                prodNameBuf.append("BG");
            }
            else if ("CI".equalsIgnoreCase(temp)) {
                prodNameBuf.append("CI");
            }
            else  { // Unknown code
                return false;
            }
        }
        // append Reserved to the product name if it is a reserved product
        if (isReserved) {
            prodNameBuf.append(" RESERVED");
            this.setVisibility("Internal");
            this.setLongDescription("  **Reserved products are in very limited supply.");
        }
        
        categoryBuf.append(pubDate.year().getAsText()).append("|").append(pubDate.monthOfYear().getAsText());
        keywordBuf.append(pubDate.monthOfYear().getAsText()).append(" ");
        keywordBuf.append(pubDate.toString("yyyy")).append(" ");
        keywordBuf.append(pubDate.toString("M/d/yyyy")).append(" ");
        keywordBuf.append(pubDate.toString("MM/dd/yyyy")).append(" ");
        keywordBuf.append(pubDate.toString("M/d/yy")).append(" ");
        keywordBuf.append(pubDate.toString("MM/dd/yy")).append(" ");
        keywordBuf.append(pubDate.toString("MM-dd-yy")).append(" ");
        keywordBuf.append(pubDate.toString("MMddyy")).append(" ");
        keywordBuf.append(pubDate.toString("MMddyyyy")).append(" ");
        keywordBuf.append(pubDate.toString("Mdyy")).append(" ");
        keywordBuf.append(pubDate.toString("Mdyyyy")).append(" ");
        keywordBuf.append(pubDate.toString("MM-dd-yyyy"));
        
        
        double priceD = qp.getBackIssuePrice();
        if (priceD == 0.0) {
            priceD = 2.0; // default to $2
        }

        this.setTaxable("All");
        this.setPrice(String.valueOf(priceD));
        this.setProductName(prodNameBuf.toString());
        this.setLongDescription(longDescriptionBuf.toString());
        this.setSku(skuBuf.toString());
        this.setPricingModel("SINGLE");
        this.setCategory(categoryBuf.toString());
        this.setCategoryOrder(String.valueOf(pubDate.getDayOfMonth()));
        this.setKeywords(keywordBuf.toString());
        if (qp.getQuantity() <= 0) {
            this.setStatus("Sold Out");
            this.setInventory("0");
        }
        else {
            this.setStatus("Normal");
            this.setInventory(String.valueOf(qp.getQuantity()));
        }
        
        // anything published before 1988 is for corporate use only
        if (pubDate.isBefore(NexternalProductBO.publicAvailabilityDate)) {
            this.setVisibility("Type:Corporate");
            this.setInternalMemo("THIS PRODUCT AVAILABLE TO CORPORATE CUSTOMERS ONLY.");
        }
        
        this.setMinimumQuantity("1");
        this.setMaximumQuantity("50");
        this.setDepletionStatus("Sold Out");
        this.setWarningLevel("25");
        this.setReviewable("No");
        this.setWeight("1");
        return true;
    }
}
