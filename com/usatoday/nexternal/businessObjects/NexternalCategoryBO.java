/*
 * Created on Mar 9, 2006
 * @copyright Copyright 2005 USA TODAY. All rights reserved.
 */
package com.usatoday.nexternal.businessObjects;

import org.joda.time.DateTime;

import com.usatoday.nexternal.interfaces.NexternalCategoryIntf;
import com.usatoday.nexternal.interfaces.QuestProductIntf;

/**
 * @author aeast
 * @date Mar 9, 2006
 * @class NexternalCategoryBO
 * 
 * This class represents a category in Nexternal
 * 
 */
public class NexternalCategoryBO implements NexternalCategoryIntf {

    private String category = "";
    private String visibility = "";
    private String thumbNail = "";
    private String thumbNailReduce = "";
    private String thumbNailOnly = "";
    private String categoryOrder = "";
    private String active = "";
    
    // 
    private static DateTime publicAvailabilityDate = new DateTime(1988, 1, 1, 0, 0, 0, 0);
    
    static final String[] monthImages = { "", "cat_january.jpg","cat_february.jpg", "cat_march.jpg", "cat_april.jpg", "cat_may.jpg", "cat_june.jpg", "cat_july.jpg", "cat_august.jpg", "cat_september.jpg", "cat_october.jpg", "cat_november.jpg", "cat_december.jpg"};
    
    
    public String getThumbNailReduce() {
        return thumbNailReduce;
    }
    public void setThumbNailReduce(String thumbNailReduce) {
        this.thumbNailReduce = thumbNailReduce;
    }
    public String getActive() {
        return this.active;
    }
    public void setActive(String active) {
        this.active = active;
    }
    public String getCategory() {
        return this.category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryOrder() {
        return this.categoryOrder;
    }
    public void setCategoryOrder(String categoryOrder) {
        this.categoryOrder = categoryOrder;
    }
    public String getThumbNail() {
        return this.thumbNail;
    }
    public void setThumbNail(String thumbNail) {
        this.thumbNail = thumbNail;
    }
    public String getThumbNailOnly() {
        return this.thumbNailOnly;
    }
    public void setThumbNailOnly(String thumbNailOnly) {
        this.thumbNailOnly = thumbNailOnly;
    }
    public String getVisibility() {
        return this.visibility;
    }
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
    public boolean equals(Object obj) {
        NexternalCategoryIntf o2 = null;
        if (obj instanceof NexternalCategoryIntf) {
            o2 = (NexternalCategoryIntf)obj;
            if (this.getCategory().equalsIgnoreCase(o2.getCategory())) {
                return true;
            }
        }
        else {
            return false;
        }
        return false;
    }
    
    /**
     * 
     * @param qp
     * @throws Exception
     */
    public boolean generateCateogryForQuestProduct(QuestProductIntf qp) throws Exception {
        if (qp == null) { 
            return false;
        }
        
        StringBuffer categoryBuf = new StringBuffer();
        String pub = qp.getPubId();
        
        String editionDate = qp.getEditionDate();
        String year = editionDate.substring(0,4);
        String month = editionDate.substring(5,7);
        String day = editionDate.substring(8);
        
        DateTime pubDate = new DateTime(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(day), 0, 0, 0, 0 );
        
        int monthInt = pubDate.getMonthOfYear();

        this.setVisibility("All");

        if ("UT".equalsIgnoreCase(pub)) {
            categoryBuf.append("USA TODAY Previous Issues|");
            
        }
        else if ("BW".equalsIgnoreCase(pub)) {
            categoryBuf.append("Sports Weekly Previous Issues|");
        }
        else if ("UW".equalsIgnoreCase(pub)){
            categoryBuf.append("USA Weekend Previous Issues|");
        }
        else if("IN".equalsIgnoreCase(pub)){
            categoryBuf.append("USA TODAY International Previous Issues|");
        }
        else {
            categoryBuf.append("Special Editions Previous Issues|");
        }
        categoryBuf.append(pubDate.year().getAsText()).append("|").append(pubDate.monthOfYear().getAsText());
        
        this.setCategory(categoryBuf.toString());
        
        // anything published before 1988 is for corporate use only
        if (pubDate.isBefore(NexternalCategoryBO.publicAvailabilityDate)) {
            this.setVisibility("Type:Corporate");
        }
        
        this.setActive("Yes");
        this.setThumbNailOnly("Yes");
        this.setCategoryOrder(String.valueOf(monthInt));
        if (monthInt > -1 && monthInt < 13) {
            this.setThumbNail(NexternalCategoryBO.monthImages[monthInt]);
        }
        
        return true;
    }
}
